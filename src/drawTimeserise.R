library(plotly)
library(RColorBrewer)
library(ggplot2)

if(!exists("serise"))
  source("src//toTimeSerise.r")

totalSerise  = xts(x = totalSerise$Number,order.by =as.POSIXct(totalSerise$Time))



plot <- plot_ly(serise, x = ~Time, y = ~Number, color = ~Type,
                marker = list(colors = brewer.pal(9,"Set1"))
                ) %>%
  add_lines(alpha = 0.7)

plot


p<-plot_ly(dailySerise, x = ~Date, y = ~Number, color = ~Type,
        marker = list(colors = brewer.pal(9,"Set1"))
        ) %>%  add_lines()
p


p2 = plot_ly(WeekSerise, x=seq(1,nrow(WeekSerise),1),y = ~Number, color = ~Type,
             marker = list(colors = brewer.pal(9,"Set1"))
) %>%  add_lines(alpha = 0.7)

p2



p3 = ggplot(dailySerise,aes(x = Date,y =Number,group = Type,color  = Type))+geom_line()+scale_fill_manual(brewer.pal(9,"Set3"))

ggplotly(p3)
















