library(dplyr)
library(plotly)
library(RColorBrewer)
library(ggplot2)
library(readr)


Category_result = read_csv(choose.files())

other = group_by(Category_result,主類別) %>% summarise(sum(數量))


Category_result2 = read_csv(choose.files())



# mainCate = as.data.frame(table(Category_result$PARENT_CATEGORY_NAME_TW))
# totalCate = as.data.frame(table(Category_result$CATEGORY_NAME_TW , Category_result$PARENT_CATEGORY_NAME_TW)) %>% filter(Freq != 0)



p <- plot_ly(other, labels = ~種類, values = ~數量, type = 'pie',
             marker = list(colors = brewer.pal(9,"Set3"))) %>%
  layout(title = '其他事件分類結果',
         showlegend = T,
         xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
         yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE)
         )
p

names(other)<-c("種類","數量","type")



b = ggplot(other,aes(x=type,y = 數量,fill = 種類))
b+geom_col()+scale_fill_brewer(palette="Set3")+theme(panel.background = element_blank(),panel.grid.major =element_line(colour = "gray")) 

export(p,"p.png")

